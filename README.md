# Terminal Diary

A small diary in your terminal!

## Installing

(You must have python3)

1. Clone the repository:
```
git clone https://codeberg.org/mskf1383/Terminal-Diary.git
```
2. Go to cloned repository directory:
```
cd Terminal-Diary
```
3. Run this command to install it:
```
python3 setup.py install
```

## Using
Write diary:
```
diary write
```

> It saved as markdown file in `Notes` folder in your home directory.

Read previous diaries:
```
diary read <date>
```

> Instead of \<date> put the date:
`today`, `yesterday` or the exact date (like `2021-1-1`).

## License

[![GNU GPL v3](./LICENSE.png)](./LICENSE)
