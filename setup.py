from setuptools import setup

setup(
    name='Terminal Diary',
    version='0.1.3',
    description='A small diary in your terminal!',
    scripts= ["diary"]
)
